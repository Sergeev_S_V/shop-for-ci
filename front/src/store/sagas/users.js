import {put} from 'redux-saga/effects';
import {push} from "react-router-redux";
import {NotificationManager} from "react-notifications";

import axios from "../../axios-api";
import {registerUserFailure, registerUserSuccess} from "../actions/users";

export function* registerUserSaga(action) {
  try {
    yield axios.post('/users', action.userData);
    yield put(registerUserSuccess());
    yield put(push('/login'));
    yield NotificationManager.success('Success', 'Registration successful');
  } catch (error) {
    yield put(registerUserFailure(error.response.data));
  }
}
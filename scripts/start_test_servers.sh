#!/bin/bash

REL_PATH=$(dirname "$0")
cd ${REL_PATH}
DIR="$(pwd)"

echo "Current dir is: ${PWD}"

cd ${DIR}/../front
pm2 start "yarn start:test" --name shop-test-front

cd ${DIR}/../api
pm2 start "yarn start:test" --name shop-test-api

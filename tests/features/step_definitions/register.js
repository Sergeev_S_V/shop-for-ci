const urls = require('./urls');

module.exports = function() {
  this.Given(/^я захожу на страницу регистрации$/, function () {
    return browser.url(urls.registerUrl);
  });

  this.When(/^я ввожу в поле "([^"]*)" значение "([^"]*)"$/, function (fieldName, value) {
    const input = browser.element(`input[name="${fieldName}"]`);
    return input.setValue(value);
  });

  this.When(/^нажимаю на кнопку "([^"]*)"$/, function (text) {
    const button = browser.element(`button=${text}`);
    return button.click();
  });

  this.Then(/^я вижу сообщение об успешной регистрации$/, function () {
    const notification = browser.element('.notification-message .title');
    notification.waitForExist(5000);

    const notificationText = browser.element('.notification-message .title').getText();

    return expect(notificationText).toBe('Registration successful');
  });
};
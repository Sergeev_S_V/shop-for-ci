#!/bin/bash

TESTS_REL_PATH=$(dirname "$0")
cd ${TESTS_REL_PATH}
TESTS_DIR="$(pwd)"

echo "## Seeding the database"
cd ${TESTS_DIR}/../api
APP_ENV=test yarn seed

echo "## Running tests"
cd ${TESTS_DIR}
yarn chimp